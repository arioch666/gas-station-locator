package indie.arioch.gasstationlocator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ariochdivij666 on 3/7/16.
 */
public class GasStationLocatorSession {

    private List<GasStation> gasStationList;

    public List<GasStation> getGasStationList() {
        if (this.gasStationList == null) {
            this.gasStationList = new ArrayList<>();
        }

        return gasStationList;
    }
}
