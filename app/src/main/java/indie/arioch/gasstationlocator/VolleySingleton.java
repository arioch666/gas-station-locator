package indie.arioch.gasstationlocator;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by ariochdivij666 on 3/7/16.
 */
public class VolleySingleton {
    private static VolleySingleton volleyInstance;
    private RequestQueue requestQueue;

    private VolleySingleton() {
        this.requestQueue = Volley.newRequestQueue(GasStationLocator.getAppContext());
    }

    public static VolleySingleton getInstance() {
        if (volleyInstance == null) {
            volleyInstance = new VolleySingleton();
        }

        return volleyInstance;
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }
}
