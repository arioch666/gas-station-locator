package indie.arioch.gasstationlocator;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ariochdivij666 on 3/7/16.
 * <p/>
 * "country": "United States"
 * "zip": "94066"
 * "reg_price": "4.09"
 * "mid_price": "4.19"
 * "pre_price": "4.29"
 * "diesel_price": "N/A"
 * "reg_date": "9 months ago"
 * "mid_date": "9 months ago"
 * "pre_date": "9 months ago"
 * "diesel_date": "9 months ago"
 * "address": "1199 El Camino Real"
 * "diesel": "1"
 * "id": "99425"
 * "lat": "37.636730"
 * "lng": "-122.421410"
 * "station": "Shell"
 * "region": "California"
 * "city": "San Bruno"
 * "distance": "0.3 miles"
 */
public class GasStation extends JSONObject {

    public GasStation(String json) throws JSONException {
        super(json);
    }

    public long getId() throws JSONException {
        return this.getLong("id");
    }

    public String getReg_price() throws JSONException {
        return this.getString("reg_price");
    }

    public String getAddress() throws JSONException {
        return this.getString("address");
    }

    public String getLat() throws JSONException {
        return this.getString("lat");
    }

    public String getLng() throws JSONException {
        return this.getString("lng");
    }

    public String getStation() throws JSONException {
        return this.getString("station");
    }

    public String getDistance() throws JSONException {
        return this.getString("distance");
    }

}
