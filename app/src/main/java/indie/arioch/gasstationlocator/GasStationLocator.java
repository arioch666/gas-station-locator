package indie.arioch.gasstationlocator;

import android.app.Application;
import android.content.Context;

/**
 * Created by ariochdivij666 on 3/7/16.
 */
public class GasStationLocator extends Application {
    private static GasStationLocator instance;
    private static Context appContext;
    private GasStationLocatorSession gasStationLocatorSession;

    public static GasStationLocator getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return appContext;
    }

    public void setAppContext(Context mAppContext) {
        this.appContext = mAppContext;
    }

    public GasStationLocatorSession getGasStationLocatorSession() {
        return gasStationLocatorSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        this.setAppContext(getApplicationContext());
        this.gasStationLocatorSession = new GasStationLocatorSession();

    }

}
