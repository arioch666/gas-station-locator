package indie.arioch.gasstationlocator;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String POSITION = MapsActivity.class + ".POSITION";
    private GoogleMap mMap;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.position = this.getIntent().getIntExtra(MapsActivity.POSITION, 0);
        mMap = googleMap;

        GasStation gasStation = GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().get(position);

        // Add a marker in Sydney and move the camera
        LatLng gasStationLatLong = null;
        try {
            gasStationLatLong = new LatLng(Double.parseDouble(gasStation.getLat()), Double.parseDouble(gasStation.getLng()));
            mMap.addMarker(new MarkerOptions().position(gasStationLatLong).title(gasStation.getStation()).draggable(false).snippet(gasStation.getAddress()).visible(true));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(gasStationLatLong).zoom(15).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
