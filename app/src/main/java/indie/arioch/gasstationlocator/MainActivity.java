package indie.arioch.gasstationlocator;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RefreshInterface refreshInterface;
    FABInterface FABInterface;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    public FloatingActionButton getFab() {
        return fab;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public FABInterface getFABInterface() {
        return FABInterface;
    }

    public void setFABInterface(FABInterface FABInterface) {
        this.FABInterface = FABInterface;
    }

    public RefreshInterface getRefreshInterface() {
        return refreshInterface;
    }

    public void setRefreshInterface(RefreshInterface refreshInterface) {
        this.refreshInterface = refreshInterface;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.this.FABInterface != null) {
                    MainActivity.this.FABInterface.updateFAB();
                } else {

                    Toast.makeText(MainActivity.this, "No Listener Allocated", Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.getFragmentManager().beginTransaction().add(R.id.frameLayoutContent, new MainActivityFragment(), "").commitAllowingStateLoss();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            if (this.refreshInterface != null) {
                this.refreshInterface.refresh();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    interface RefreshInterface {
        void refresh();

    }

    interface FABInterface {
        void updateFAB();

    }
}
