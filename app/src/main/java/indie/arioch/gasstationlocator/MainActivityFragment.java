package indie.arioch.gasstationlocator;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener, MainActivity.RefreshInterface, AdapterView.OnItemClickListener, MainActivity.FABInterface {

    private MainActivity mainActivity;
    private ListViewAdapter listViewAdapter;
    private ListView listView;
    private double lat;
    private double lng;
    private boolean isSortedByDistance = false;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        this.listView = (ListView) view.findViewById(R.id.listView);

        this.listView.setOnItemClickListener(this);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (this.mainActivity == null) {
            this.mainActivity = (MainActivity) this.getActivity();
        }

        if (this.listViewAdapter == null) {
            this.listViewAdapter = new ListViewAdapter();
        }

        this.listView.setAdapter(this.listViewAdapter);

        if (GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().size() == 0) {

            if (this.mainActivity.getRefreshInterface() == null) {
                this.mainActivity.setRefreshInterface(this);
                this.mainActivity.setFABInterface(this);
            }
            this.makeRequest();

        }


    }

    public void makeRequest() {
        LocationManager locationManager = (LocationManager) GasStationLocator.getAppContext().getSystemService(Context.LOCATION_SERVICE);

        String locationProvider = LocationManager.GPS_PROVIDER;

        Location location = locationManager.getLastKnownLocation(locationProvider);


        this.lat = location.getLatitude();
        this.lng = location.getLongitude();

        Toast.makeText(this.mainActivity, this.isSortedByDistance ? "Sorted by distance" : "Sorted by price", Toast.LENGTH_SHORT).show();

        String url = this.isSortedByDistance ? "http://api.mygasfeed.com/stations/radius/" + this.lat + "/" + this.lng + "/2/reg/distance/wcplzhec0w.json" : "http://api.mygasfeed.com/stations/radius/" + this.lat + "/" + this.lng + "/2/reg/price/wcplzhec0w.json";

        GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().clear();
        this.listViewAdapter.notifyDataSetChanged();
        JsonObjectRequest jsonRequest;
        jsonRequest = new JsonObjectRequest(Request.Method.GET, url, this, this);

        Log.d("JsonRequest", jsonRequest.toString());

        Volley.newRequestQueue(this.mainActivity).add(jsonRequest);


    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray("stations");
            Toast.makeText(this.mainActivity, "List Size:" + jsonArray.length(), Toast.LENGTH_SHORT).show();
            for (int looper = 0; looper < jsonArray.length(); looper++) {
                JSONObject jsonObject = jsonArray.getJSONObject(looper);

                GasStation gasStation = new GasStation(jsonObject.toString());
                GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().add(gasStation);
            }

            this.listViewAdapter.notifyDataSetChanged();

            View view = this.getView();
            FrameLayout frameLayoutProgress = (FrameLayout) view.findViewById(R.id.frameLayoutProgress);
            frameLayoutProgress.setVisibility(View.GONE);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this.mainActivity, "Error fetching data", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void refresh() {
        this.getView().findViewById(R.id.frameLayoutProgress).setVisibility(View.VISIBLE);

        this.makeRequest();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent myIntent = new Intent(this.mainActivity, MapsActivity.class);
        myIntent.putExtra(MapsActivity.POSITION, position); //Optional parameters
        this.mainActivity.startActivity(myIntent);

    }

    @Override
    public void updateFAB() {

        android.support.v7.widget.Toolbar toolbar = this.mainActivity.getToolbar();
        View view = this.getView();

        if (this.isSortedByDistance) {
            this.isSortedByDistance = false;

            this.makeRequest();

            this.mainActivity.getFab().setBackgroundDrawable(this.mainActivity.getResources().getDrawable(android.R.drawable.ic_dialog_map));


        } else {

            this.isSortedByDistance = true;


            this.makeRequest();

            this.mainActivity.getFab().setBackgroundDrawable(this.mainActivity.getResources().getDrawable(android.R.drawable.ic_menu_sort_by_size));
        }

        assert view != null;
        view.findViewById(R.id.frameLayoutProgress).setVisibility(View.VISIBLE);
    }

    private class ListViewAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().size();
        }

        @Override
        public Object getItem(int position) {
            return GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().get(position);
        }

        @Override
        public long getItemId(int position) {
            try {
                return GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().get(position).getId();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = MainActivityFragment.this.mainActivity.getLayoutInflater().inflate(R.layout.gasstationlistitem, parent, false);

            TextView textViewAddress = (TextView) convertView.findViewById(R.id.textViewAddress);
            TextView textViewDistance = (TextView) convertView.findViewById(R.id.textViewDistance);
            TextView textViewStation = (TextView) convertView.findViewById(R.id.textViewStation);
            TextView textViewRegularPrice = (TextView) convertView.findViewById(R.id.textViewRegularPrice);

            GasStation gasStation = GasStationLocator.getInstance().getGasStationLocatorSession().getGasStationList().get(position);

            try {
                textViewAddress.setText(gasStation.getAddress());
                textViewDistance.setText(gasStation.getDistance());
                textViewRegularPrice.setText(gasStation.getReg_price());
                textViewStation.setText(gasStation.getStation());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
